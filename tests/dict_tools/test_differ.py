from dict_tools import data
from dict_tools import differ

NONE = differ.RecursiveDictDiffer.NONE_VALUE


class TestRecursiveDiffer:
    def setup_method(self):
        old_dict = {
            "a": {"b": 1, "c": 2, "e": "old_value", "f": "old_key"},
            "j": "value",
        }
        new_dict = {
            "a": {"b": 1, "c": 4, "e": "new_value", "g": "new_key"},
            "h": "new_key",
            "i": None,
            "j": "value",
        }
        self.recursive_diff = differ.recursive_diff(
            old_dict, new_dict, ignore_missing_keys=False
        )
        self.recursive_diff_ign = differ.recursive_diff(old_dict, new_dict)

    def teardown_method(self):
        for attrname in ("recursive_diff", "recursive_diff_missing_keys"):
            try:
                delattr(self, attrname)
            except AttributeError:
                continue

    def test_added(self):
        assert self.recursive_diff.added() == ["a.g", "h", "i"]

    def test_removed(self):
        assert self.recursive_diff.removed() == ["a.f"]

    def test_changed_with_ignore_unset_values(self):
        self.recursive_diff.ignore_unset_values = True
        assert self.recursive_diff.changed() == ["a.c", "a.e"]

    def test_changed_without_ignore_unset_values(self):
        self.recursive_diff.ignore_unset_values = False
        assert self.recursive_diff.changed() == ["a.c", "a.e", "a.f", "a.g", "h", "i"]

    def test_unchanged(self):
        assert self.recursive_diff.unchanged() == ["a.b", "j"]

    def test_diffs(self):
        assert self.recursive_diff.diffs == {
            "a": {
                "c": {"old": 2, "new": 4},
                "e": {"old": "old_value", "new": "new_value"},
                "f": {"old": "old_key", "new": NONE},
                "g": {"old": NONE, "new": "new_key"},
            },
            "h": {"old": NONE, "new": "new_key"},
            "i": {"old": NONE, "new": None},
        }
        assert self.recursive_diff_ign.diffs == {
            "a": {
                "c": {"old": 2, "new": 4},
                "e": {"old": "old_value", "new": "new_value"},
                "g": {"old": NONE, "new": "new_key"},
            },
            "h": {"old": NONE, "new": "new_key"},
            "i": {"old": NONE, "new": None},
        }

    def test_new_values(self):
        assert self.recursive_diff.new_values == {
            "a": {"c": 4, "e": "new_value", "f": NONE, "g": "new_key"},
            "h": "new_key",
            "i": None,
        }

    def test_old_values(self):
        assert self.recursive_diff.old_values == {
            "a": {"c": 2, "e": "old_value", "f": "old_key", "g": NONE},
            "h": NONE,
            "i": NONE,
        }

    def test_changes_str(self):
        assert self.recursive_diff.changes_str == (
            "a:\n"
            "  c from 2 to 4\n"
            "  e from 'old_value' to 'new_value'\n"
            "  f from 'old_key' to nothing\n"
            "  g from nothing to 'new_key'\n"
            "h from nothing to 'new_key'\n"
            "i from nothing to None"
        )


def test_namespace_dict():
    source_dict = dict(thing_1="thing_2", a="b")
    name_dict = data.NamespaceDict(thing_1="thing_2", b="c")
    result = differ.deep_diff(source_dict, name_dict)
    assert isinstance(result["old"], dict)
    assert isinstance(result["new"], dict)


def test_dict_with_lists():
    source_dict = dict(
        thing_1="thing_2",
        a="b",
        list_1=[1, 2, 3, 4, 5],
        list_2=["hello"],
        list_4=["not", "in", "name"],
    )
    name_dict = data.NamespaceDict(
        thing_1="thing_2",
        b="c",
        list_1=[2, 3, 4, 4],
        list_2=["world"],
        list_3=["not", "in", "source"],
    )
    result = differ.deep_diff(source_dict, name_dict)
    assert isinstance(result["old"], dict)
    assert isinstance(result["new"], dict)
    assert result["old"]["list_1"] == [1, 5]
    assert result["new"]["list_1"] == [4]
    assert result["old"]["list_2"] == ["hello"]
    assert result["new"]["list_2"] == ["world"]
